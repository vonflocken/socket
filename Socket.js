const MESSAGE_SEPARATOR = ' '
const MESSAGE_TYPE_REPLY = '_reply'

export default class Socket {
  // stores registered event listeners
  events = {}
  waiting_messages = {}

  // `socket`: WebSocket object
  constructor(socket) {
    this.ws = socket
    // browser compatibility
    if (!this.ws.on) this.ws.on = this.ws.addEventListener
    this.ws.on('message', (msg) => {
      // browser compatibility
      msg = msg.data || msg.toString()
      const [event_name, message_id, reply_id] = msg.split(MESSAGE_SEPARATOR, 3)
      // we're only checking for `message_id` here, since `reply_id` could be an empty string
      if (!message_id) return

      const is_reply = event_name === MESSAGE_TYPE_REPLY
      // check if we're actually listening for this event
      if (is_reply) {
        if (!reply_id) return
        if (!(reply_id in this.waiting_messages)) return
      } else {
        if (!(event_name in this.events)) return
      }

      // parse payload
      const payload_start_index =
        event_name.length +
        message_id.length +
        reply_id.length +
        MESSAGE_SEPARATOR.length * 3
      let payload
      try {
        payload = JSON.parse(msg.substring(payload_start_index))
      } catch (e) {
        return
      }

      // helper that resolves the promise / calls the callback function
      const handle_callback = (message) =>
        message.resolve({
          event_name,
          payload,
          reply: (reply_payload, timeout = 0) =>
            this.#send(
              MESSAGE_TYPE_REPLY,
              crypto.randomUUID(),
              message_id,
              reply_payload,
              timeout,
            ),
        })

      if (is_reply) {
        handle_callback(this.waiting_messages[reply_id])
        delete this.waiting_messages[reply_id]
      } else {
        ;(this.events[event_name] || []).forEach(handle_callback)
      }
    })
  }

  // interface for registering event listeners
  on(event_name, callback) {
    // check for collisions
    if (event_name.includes(MESSAGE_SEPARATOR))
      throw new Error(`invalid separator character in event name "${event_name}"`)
    if (event_name === MESSAGE_TYPE_REPLY)
      throw new Error(`illegal event name: ${event_name}`)

    // add listener to list of registered event listeners
    if (!(event_name in this.events)) this.events[event_name] = []
    this.events[event_name].push({ resolve: callback })
  }

  // interface for sending messages
  send(event_name, payload = {}, timeout = 0) {
    // check for collisions
    if (event_name.includes(MESSAGE_SEPARATOR))
      throw new Error(`invalid separator character in event name "${event_name}"`)
    if (event_name === MESSAGE_TYPE_REPLY)
      throw new Error(`illegal event name: ${event_name}`)

    return this.#send(event_name, crypto.randomUUID(), '', payload, timeout)
  }

  // this private method actually sends the message
  #send(event_name, id, reply_id, payload, timeout) {
    this.ws.send(`${event_name}${MESSAGE_SEPARATOR}${id}${MESSAGE_SEPARATOR}${reply_id}${MESSAGE_SEPARATOR}${JSON.stringify(payload)}`)
    if (timeout === 0) return

    return new Promise((resolve, reject) => {
      this.waiting_messages[id] = { resolve, reject }
      // negative timeout means wait indefinitely
      if (timeout > 0)
        setTimeout(() => {
          delete this.waiting_messages[id]
          resolve({ error: 'timed out' })
        }, timeout)
    })
  }
}
